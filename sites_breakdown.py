# Sites breakdown

# Article keywords
keywords = ['foco', 'corrida', 'treinos', 'ligamentos', 'ligamentos', 'joelho', 'oxigenoterapia', 'recuperação',
'tendão de aquiles', 'correr', 'exercícios', 'fortalecer', 'esportistas', 'corredores', 'benefícios',
'maratona', 'treinamento', 'abdominal', 'musculação', 'ciclistas', 'aeróbicos', 'anaeróbicos', 'bike', 'perca peso',
'crossfit', 'joelheira', 'workouts', 'exercício', 'bodybuilder', 'triathlon', 'hiit', 'endurance', 'bcaa', 
'chá verde', 'triatleta', 'tênis', 'ritmo', 'fortalecimento', 'trilhas', 'corrida de montanha', 'trail run', 'hidratar',
'natação', 'piscina', 'muscular', 'alimentação', 'ironman', 'dieta', 'vegana', 'atletas', 'imunidade', 'proteína', 'vegetal'
'ginseng', 'cardápio', 'massa muscular', 'low carb', 'colágeno', 'pré-treino', 'índice glicêmico', 'saúde', 'aveia', 'receita',
'pós-treino', 'água com limão', 'kefir', 'fitness', 'massa magra', 'treinamento', 'academia', 'calistenia', 'perder barriga',
'queima', 'emagrecer', 'estímulos', 'abdômen', 'treinar', 'saudáveis', 'exercício', 'glúteos', 'cinta modeladora', 'treino',
'esporte', 'fisioterapia', 'yoga', 'alimentos', 'retenção de líquidos', 'atividade física']

# Blacklisted words
blacklist = ['ativo coach']


# Create pagelink types
page_link_types = [
    'base_url',
    'running_training',
    'running_tips',
    'cycling_training',
]

# Create a dictionary for each site and their pagelinks
ativo = {
    'base_url': 'https://www.ativo.com/',
    'running_training': 'https://www.ativo.com/corrida-de-rua/treinamento-de-corrida/',
    'cycling_training': 'https://www.ativo.com/bike/treinamento/',
    'crossfit_training': 'https://www.ativo.com/cross-training/treinamento-cross-training/',
    'triathalon_training': 'https://www.ativo.com/triathlon/treinamento-triathlon/',
    'mountain_climbing_training': 'https://www.ativo.com/corrida-de-montanha/treinamento-corrida-de-montanha/',
    'swimming_training': 'https://www.ativo.com/natacao/treinamento-natacao/',
    'nutrition': 'https://www.ativo.com/nutricao/',
    'fitness_training': 'https://www.ativo.com/fitness/treinamento-fitness/',
    'for_women': 'https://www.ativo.com/mulher/',
}

# Add all sites to a list
all_sites = [ativo]