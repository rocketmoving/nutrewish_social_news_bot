#Nutrewish Social News Bot#

**A bot to extract article posts from different websites and share on social media.**

Written in Python 3.7.3 🐍

If you've enjoyed this content, give us a shoutout on Instagram: [@rocketmovingapp](https://www.instagram.com/rocketmovingapp) 

*View more content on our [Rocket Moving App development blog](https://rocketmoving.dev).*

---

## Notes

This bot currently works with the following sites:

1. www.ativo.com
**Running | Training**: [Corrida de Rua | Treinamento](https://www.ativo.com/corrida-de-rua/treinamento-de-corrida/)
**Cycling | Training**: [Bike | Treinamento](https://www.ativo.com/bike/treinamento/)
**Crossfit | Training**: [Crossfit | Treinamento](https://www.ativo.com/cross-training/treinamento-cross-training/)
**Triathalon | Training**: [Triathalon | Treinamento](https://www.ativo.com/triathlon/treinamento-triathlon/)
**Mountain Climbing | Training**: [Corrida de Montanha | Treinamento](https://www.ativo.com/corrida-de-montanha/treinamento-corrida-de-montanha/)
**Swimming | Training**: [Natacao | Treinamento](https://www.ativo.com/natacao/treinamento-natacao/)
**Nutrition | Training**: [Nutricao | Treinamento](https://www.ativo.com/nutricao/)
**Fitness | Training**: [Fitness | Treinamento](https://www.ativo.com/fitness/treinamento-fitness/)
**For Women | Training**: [Mulheres | Treinamento](https://www.ativo.com/mulher/)

---

## Requirements

1. Install a virtual terminal (Optional)
*Windows: 'python -m venv venv'*

2. Install the Selenium library
*Windows: 'pip install selenium'*

3. Install the Google Client Library
**A Google Cloud Console account will be required to access Google's APIs**
*Windows: 'pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib'*