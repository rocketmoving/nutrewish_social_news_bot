# Bot to get latest news from sites

# Import modules
from sites_breakdown import all_sites, keywords, blacklist
from time import sleep
import os

# Import site specific modules
from ativo_site import get_ativo


# Import libraries for Selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

# Set path to geckodriver and create browser webdriver
geckodriver_path = r'C:\Projects\geckodriver_win64\geckodriver.exe'
browser = webdriver.Firefox(executable_path=geckodriver_path)


# Get each site
for site in all_sites:
    # Get first site
    site_page = list(site.values())[1]
    print('Mining data from website: ', site_page)
    page = browser.get(site_page)
    sleep(3)

    if 'www.ativo.com' in site_page:
        extracted_articles = get_ativo(browser)
        save_content(extracted_articles)

