# Modules specific to https://www.ativo.com sites
"""
from datetime import date
# Find today's date
today = date.today()
# mm/dd/y
posted_on = today.strftime("%m/%d/%y")
"""
# Import libraries
from time import sleep
from sites_breakdown import keywords, blacklist

def get_ativo(browser):
    """Function to interact with Ativo sites."""

    def close_pop_up(browser):
        """Function to check for pop up notifications and close them."""

        # Look for pop up window
        try:
            find_pop_up = browser.find_element_by_id('fechar')
            pop_up = find_pop_up.find_element_by_tag_name('a')
            pop_up.click()
            if pop_up:
                print(f'Closing pop up')

            else:
                pass
        except Exception as NoPopUpError:
            sleep(5)
            print(f'No pop up found')    

    # Close any pop up
    close_pop_up(browser)
    # Site name
    site_name = 'Ativo'
    # Find main column
    main_column = browser.find_element_by_id('conteudo')
    # Find all article titles in main column
    #article_titles = main_column.find_elements_by_xpath('//h1[contains(text(), "")]')
    
    # Find all article links in main column
    articles = main_column.find_elements_by_xpath('//h1[contains(text(), "")]/parent::node()')
    # Create an empty list
    extracted_articles = []
    for article in articles:
        # Create a dictionary
        article_title = article.get_attribute('title')
        
        # Check for keyword
        for keyword in keywords:
            if keyword in article_title.lower():
                print(f'Found keyword {keyword}. Saving article.')
                keep = True
                break
            elif keyword not in article_title.lower():
                print(f'Skipping article due to no keyword')
                keep = False

        # Check for blacklisted words
        for blacklist_word in blacklist:
            if blacklist_word in article_title.lower():
                print(f'Skipping article due to blacklist word: {blacklist_word}')
                keep = False
                break
            # If not blacklisted, check for keyword
            elif blacklist_word not in article_title.lower():
                pass
            
        # Store if true
        if keep == True:
            this_article = {}
            this_article.update({'article_title': article_title},)
            this_article.update({'article_link': article.get_attribute('href')},)
            print(f"Extracting: {this_article['article_title']}")
            extracted_articles.append(this_article)
        
        sleep(1)

    #print(f'From site {site_name}"\n\n"')
    print(f'Extracted a total of {str(len(extracted_articles))} articles.')

    return extracted_articles

